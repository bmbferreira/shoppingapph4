import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.service.ServiceRegistry;

public class Main {
  private static SessionFactory sessionFactory;

  public static SessionFactory createSessionFactory() {
    Configuration configuration = new Configuration();
    configuration.configure();
    ServiceRegistry serviceRegistry =
        new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    return sessionFactory;
  }

  public static SessionFactory getSessionFactory() {
    return createSessionFactory();
  }

  public static void shutdown() {
    // Close caches and connection pools
    getSessionFactory().close();
  }

  public static void main(String[] args) {
    Session session = getSessionFactory().openSession();
    Transaction t = session.beginTransaction();
    Store s = (Store) session.get(Store.class, 1);
    try {
      session.delete(s);
      t.commit();
    } catch (ConstraintViolationException cException) {
      t.rollback();
      t = session.beginTransaction();
      s.setActive(false);
      session.update(s);
      t.commit();
    } finally {
      shutdown();
    }
  }
}
